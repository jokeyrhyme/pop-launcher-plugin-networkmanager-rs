use std::process::Stdio;

use tokio::process::Command;

#[derive(Clone, Debug, PartialEq)]
pub(crate) struct Connection {
    pub name: String,
    pub state: State,
    pub r#type: Type,
}

#[derive(Clone, Debug, PartialEq)]
pub(crate) enum State {
    Activated,
    Deactivated,
    Other(String),
}
impl From<&str> for State {
    fn from(source: &str) -> Self {
        match source {
            "activated" => Self::Activated,
            "--" => Self::Deactivated,
            other => Self::Other(String::from(other)),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub(crate) enum Type {
    Bridge,
    Ethernet,
    Other(String),
    Vpn,
    WiFi,
}
impl From<&str> for Type {
    fn from(source: &str) -> Self {
        match source {
            "bridge" => Self::Bridge,
            "ethernet" => Self::Ethernet,
            "wifi" => Self::WiFi,
            "vpn" => Self::Vpn,
            other => Self::Other(String::from(other)),
        }
    }
}

pub(crate) fn nmcli_connection_down(network: &str) {
    // TODO: use dbus API instead of potentially-fragile CLI
    if let Err(e) = Command::new("nmcli")
        .args(["connection", "down", network])
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .spawn()
    {
        eprintln!("unable to call `nmcli`: {:?}", e);
    }
}

pub(crate) async fn nmcli_connection_show() -> Vec<Connection> {
    // TODO: use dbus API instead of potentially-fragile CLI
    let stdout = match Command::new("nmcli")
        .args([
            "--color",
            "no",
            "--fields",
            "NAME,TYPE,STATE",
            "connection",
            "show",
        ])
        .output()
        .await
    {
        Ok(o) => String::from_utf8_lossy(&o.stdout).into_owned(),
        Err(_e) => String::new(),
    };
    parse_nmcli_connection_show(stdout)
}

pub(crate) fn nmcli_connection_up(network: &str) {
    // TODO: use dbus API instead of potentially-fragile CLI
    if let Err(e) = Command::new("nmcli")
        .args(["connection", "up", network])
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .spawn()
    {
        eprintln!("unable to call `nmcli`: {:?}", e);
    }
}

pub(crate) fn nmcli_device_wifi_connect(network: &str) {
    // TODO: use dbus API instead of potentially-fragile CLI
    if let Err(e) = Command::new("nmcli")
        .args(["device", "wifi", "connect", network])
        .stdin(Stdio::null())
        .stdout(Stdio::null())
        .spawn()
    {
        eprintln!("unable to call `nmcli`: {:?}", e);
    }
}

pub(crate) async fn nmcli_device_wifi_list_noscan() -> Vec<Connection> {
    // TODO: use dbus API instead of potentially-fragile CLI
    // TODO: capture signal strength so we can sort by connection quality
    let stdout = match Command::new("nmcli")
        .args([
            "--color",
            "no",
            "--fields",
            "IN-USE,SSID",
            "device",
            "wifi",
            "list",
            "--rescan",
            "no",
        ])
        .output()
        .await
    {
        Ok(o) => String::from_utf8_lossy(&o.stdout).into_owned(),
        Err(_e) => String::new(),
    };
    parse_nmcli_device_wifi_list(stdout)
}

pub(crate) async fn nmcli_device_wifi_list_rescan() -> Vec<Connection> {
    // TODO: use dbus API instead of potentially-fragile CLI
    // TODO: capture signal strength so we can sort by connection quality
    let stdout = match Command::new("nmcli")
        .args([
            "--color",
            "no",
            "--fields",
            "IN-USE,SSID",
            "device",
            "wifi",
            "list",
            "--rescan",
            "auto",
        ])
        .output()
        .await
    {
        Ok(o) => String::from_utf8_lossy(&o.stdout).into_owned(),
        Err(_e) => String::new(),
    };
    parse_nmcli_device_wifi_list(stdout)
}

fn parse_nmcli_connection_show(stdout: String) -> Vec<Connection> {
    let type_index = stdout.find("TYPE");
    let state_index = stdout.find("STATE");
    let mut connections = vec![];
    if let (Some(type_index), Some(state_index)) = (type_index, state_index) {
        for line in stdout
            .lines()
            .skip(1) // skip header row
            .filter(|l| !l.trim().is_empty())
        {
            connections.push(Connection {
                name: String::from(line[0..type_index].trim()),
                state: State::from(line[state_index..].trim()),
                r#type: Type::from(line[type_index..state_index].trim()),
            });
        }
    }
    connections
}

fn parse_nmcli_device_wifi_list(stdout: String) -> Vec<Connection> {
    let ssid_index = stdout.find("SSID");
    let mut connections = vec![];
    if let Some(ssid_index) = ssid_index {
        for line in stdout
            .lines()
            .skip(1) // skip header row
            .filter(|l| !l.trim().is_empty())
        {
            connections.push(Connection {
                name: String::from(line[ssid_index..].trim()),
                state: if line.starts_with('*') {
                    State::Activated
                } else {
                    State::Deactivated
                },
                r#type: Type::WiFi,
            });
        }
    }
    connections
}

/// produce a list of unique connections, favouring active connections
#[allow(dead_code)]
pub(crate) fn unique_connections(connections: Vec<Connection>) -> Vec<Connection> {
    let mut uniques: Vec<Connection> = vec![];
    for connection in connections.into_iter() {
        if let Some((i, _)) = uniques
            .iter()
            .enumerate()
            .find(|(_, c)| connection.name == c.name && connection.r#type == c.r#type)
        {
            if connection.state == State::Activated {
                uniques[i].state = State::Activated;
            }
        } else {
            uniques.push(connection);
        }
    }
    uniques
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_nmcli_connection_show_with_input() {
        let input = String::from(
            r#"
NAME                           TYPE      STATE
Intel I211AT Gigabit Ethernet  ethernet  activated
docker0                        bridge    activated
MY_NETWORK                     wifi      activated
docker0                        bridge    --
docker0                        bridge    --
Mobile Hotspot                 wifi      --
Office Tunnel                  vpn       --
"#
            .trim(),
        );
        let want = vec![
            Connection {
                name: String::from("Intel I211AT Gigabit Ethernet"),
                state: State::Activated,
                r#type: Type::Ethernet,
            },
            Connection {
                name: String::from("docker0"),
                state: State::Activated,
                r#type: Type::Bridge,
            },
            Connection {
                name: String::from("MY_NETWORK"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("docker0"),
                state: State::Deactivated,
                r#type: Type::Bridge,
            },
            Connection {
                name: String::from("docker0"),
                state: State::Deactivated,
                r#type: Type::Bridge,
            },
            Connection {
                name: String::from("Mobile Hotspot"),
                state: State::Deactivated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("Office Tunnel"),
                state: State::Deactivated,
                r#type: Type::Vpn,
            },
        ];

        let got = parse_nmcli_connection_show(input);

        assert_eq!(want, got);
    }

    #[test]
    fn parse_nmcli_device_wifi_list_with_input() {
        let input = String::from(
            r#"
IN-USE  SSID
*       MY_NETWORK
        MY_NETWORK

        Buffy & Angel
        Provider 1234
"#
            .trim(),
        );
        let want = vec![
            Connection {
                name: String::from("MY_NETWORK"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("MY_NETWORK"),
                state: State::Deactivated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("Buffy & Angel"),
                state: State::Deactivated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("Provider 1234"),
                state: State::Deactivated,
                r#type: Type::WiFi,
            },
        ];

        let got = parse_nmcli_device_wifi_list(input);

        assert_eq!(want, got);
    }

    #[test]
    fn unique_connections_with_input() {
        let input = vec![
            Connection {
                name: String::from("Intel I211AT Gigabit Ethernet"),
                state: State::Activated,
                r#type: Type::Ethernet,
            },
            Connection {
                name: String::from("docker0"),
                state: State::Activated,
                r#type: Type::Bridge,
            },
            Connection {
                name: String::from("MY_NETWORK"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("docker0"),
                state: State::Deactivated,
                r#type: Type::Bridge,
            },
            Connection {
                name: String::from("docker0"),
                state: State::Deactivated,
                r#type: Type::Bridge,
            },
            Connection {
                name: String::from("Mobile Hotspot"),
                state: State::Deactivated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("Office Tunnel"),
                state: State::Deactivated,
                r#type: Type::Vpn,
            },
            Connection {
                name: String::from("MY_NETWORK"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("MY_NETWORK"),
                state: State::Deactivated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("Buffy & Angel"),
                state: State::Deactivated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("Provider 1234"),
                state: State::Deactivated,
                r#type: Type::WiFi,
            },
        ];
        let want = vec![
            Connection {
                name: String::from("Intel I211AT Gigabit Ethernet"),
                state: State::Activated,
                r#type: Type::Ethernet,
            },
            Connection {
                name: String::from("docker0"),
                state: State::Activated,
                r#type: Type::Bridge,
            },
            Connection {
                name: String::from("MY_NETWORK"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("Mobile Hotspot"),
                state: State::Deactivated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("Office Tunnel"),
                state: State::Deactivated,
                r#type: Type::Vpn,
            },
            Connection {
                name: String::from("Buffy & Angel"),
                state: State::Deactivated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("Provider 1234"),
                state: State::Deactivated,
                r#type: Type::WiFi,
            },
        ];

        let got = unique_connections(input);

        assert_eq!(want, got);
    }
}
