#![allow(dead_code)]
use crate::nm::{Connection, Type};

/// sorts so that the most relevant is first, least relevant is last
pub(crate) fn sort(connections: &mut [Connection], query: &[&str]) {
    connections.sort_by_key(|c| score(c, query))
}

/// most relevant -> most negative value
fn score(connection: &Connection, query: &[&str]) -> i16 {
    if query.is_empty() {
        return 0;
    }
    let mut result: i16 = 0;
    let query_type = Type::from(query[0]);
    if query_type == connection.r#type {
        result -= 10;
    }
    for term in query {
        if connection.name.contains(term) {
            result -= 5;
        }
    }
    result
}

#[cfg(test)]
mod tests {
    use crate::nm::State;

    use super::*;

    #[test]
    fn sort_with_no_terms() {
        let mut input = vec![
            Connection {
                name: String::from("abcdef"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("ghijkl"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("mnopqr"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
        ];
        let want = input.clone();

        sort(&mut input, &[""]);

        assert_eq!(input, want);
    }

    #[test]
    fn sort_with_type_query() {
        let mut input = vec![
            Connection {
                name: String::from("abcdef"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("ghijkl"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("mnopqr"),
                state: State::Activated,
                r#type: Type::Vpn,
            },
        ];
        let want = vec![
            Connection {
                name: String::from("mnopqr"),
                state: State::Activated,
                r#type: Type::Vpn,
            },
            Connection {
                name: String::from("abcdef"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("ghijkl"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
        ];

        sort(&mut input, &["vpn"]);

        assert_eq!(input, want);
    }

    #[test]
    fn sort_with_name_substring_query() {
        let mut input = vec![
            Connection {
                name: String::from("abcdef"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("ghijkl"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("mnopqr"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
        ];
        let want = vec![
            Connection {
                name: String::from("mnopqr"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("abcdef"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
            Connection {
                name: String::from("ghijkl"),
                state: State::Activated,
                r#type: Type::WiFi,
            },
        ];

        sort(&mut input, &["mno"]);

        assert_eq!(input, want);
    }
}
