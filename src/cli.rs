use std::{env, io};

use tokio::fs;

pub(crate) async fn install() -> io::Result<()> {
    let plugins_dir_part = pop_launcher::LOCAL_PLUGINS
        .strip_prefix("~/")
        .unwrap_or(pop_launcher::LOCAL_PLUGINS);
    let target_dir = match dirs::home_dir() {
        Some(home_dir) => home_dir.join(plugins_dir_part).join("networkmanager"),
        None => {
            return Err(io::Error::new(
                io::ErrorKind::NotFound,
                "cannot find user's home directory",
            ));
        }
    };
    let current_exe = env::current_exe()?;

    eprintln!("installing into {} ...", &target_dir.display());

    fs::create_dir_all(&target_dir).await?;
    fs::copy(current_exe, target_dir.join(env!("CARGO_PKG_NAME"))).await?;
    fs::write(target_dir.join("plugin.ron"), include_str!("../plugin.ron")).await?;

    eprintln!("... done!");

    Ok(())
}

pub(crate) fn print_usage() {
    eprintln!(
        r#"
usage:
    no arguments    ->  run as pop-launcher plugin
    install --user  ->  copy into ~/.local/share/pop-launcher/plugins
"#
    );
}
