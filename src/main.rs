#![deny(clippy::all, unsafe_code)]
// infrastructure copied from GPL-3.0-only https://github.com/pop-os/launcher/blob/master/plugins/src/scripts/mod.rs

mod cli;
mod nm;
mod search;

use std::{env, io, process::ExitCode};

use futures::{AsyncWrite, AsyncWriteExt, StreamExt};
use pop_launcher::{
    async_stdin, async_stdout, json_input_stream, PluginResponse, PluginSearchResult, Request,
};
use search::sort;

#[tokio::main]
async fn main() -> io::Result<ExitCode> {
    if let (Some(_), Some(_)) = (
        env::args().find(|arg| arg == "install"),
        env::args().find(|arg| arg == "--user"),
    ) {
        cli::install().await?;
        return Ok(ExitCode::SUCCESS);
    }
    if env::args().len() > 1 {
        eprintln!("error: unexpected arguments");
        cli::print_usage();
        return Ok(ExitCode::FAILURE);
    }

    let mut requests = json_input_stream(async_stdin());
    let mut responses = async_stdout();

    let mut connections = vec![];
    connections.extend(nm::nmcli_connection_show().await);
    connections.extend(nm::nmcli_device_wifi_list_noscan().await);
    connections = nm::unique_connections(connections);

    while let Some(result) = requests.next().await {
        if let Ok(request) = result {
            match request {
                Request::Activate(id) => {
                    let response = PluginResponse::Close;
                    send(&mut responses, response).await;
                    if let Ok(id) = id.try_into() {
                        if let Some(connection) = connections.get::<usize>(id) {
                            if connection.state == nm::State::Activated {
                                nm::nmcli_connection_down(&connection.name);
                            } else if connection.r#type == nm::Type::WiFi {
                                nm::nmcli_device_wifi_connect(&connection.name);
                            } else {
                                nm::nmcli_connection_up(&connection.name);
                            }
                        }
                    }
                }
                Request::Exit => {
                    break;
                }
                Request::Search(query) => {
                    let query = query.to_ascii_lowercase();
                    if query.starts_with("wifi") {
                        connections = vec![];
                        connections.extend(nm::nmcli_connection_show().await);
                        connections.extend(nm::nmcli_device_wifi_list_rescan().await);
                        connections = nm::unique_connections(connections);
                    }
                    let terms: Vec<&str> = query.split(' ').collect();
                    sort(&mut connections, &terms);

                    for (id, connection) in connections.iter().enumerate() {
                        let name = connection.name.to_ascii_lowercase();
                        if terms.iter().any(|t| name.contains(t)) {
                            let result = PluginSearchResult {
                                id: id.try_into().expect("cannot convert number"),
                                name: String::from(&connection.name),
                                description: format!(
                                    "{} '{}' ({:?})",
                                    if connection.state == nm::State::Activated {
                                        "Disconnect from"
                                    } else {
                                        "Connect to"
                                    },
                                    &connection.name,
                                    connection.r#type
                                ),
                                keywords: Some(vec![
                                    String::from("conn"),
                                    String::from("connection"),
                                    String::from("nm"),
                                    String::from("network"),
                                    format!("{:?}", connection.r#type).to_ascii_lowercase(),
                                ]),
                                // TODO: set icon
                                ..Default::default()
                            };
                            let response = PluginResponse::Append(result);
                            send(&mut responses, response).await;
                        }
                    }
                    let response = PluginResponse::Finished;
                    send(&mut responses, response).await;
                }
                _ => {}
            }
        }
    }

    Ok(ExitCode::SUCCESS)
}

// from GPL-3.0-only https://github.com/pop-os/launcher/blob/master/plugins/src/lib.rs
async fn send<W: AsyncWrite + Unpin>(tx: &mut W, response: PluginResponse) {
    let mut payload = serde_json::to_string(&response).expect("unable to encode plugin response");
    payload.push('\n');
    tx.write_all(payload.as_bytes())
        .await
        .expect("unable to send plugin response");
}
