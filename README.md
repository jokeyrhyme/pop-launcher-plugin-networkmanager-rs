# pop-launcher-plugin-networkmanager

plugin for pop-launcher to control NetworkManager

## features

- search for "nm" to see all available NetworkManager commands

### Wi-Fi

- returns all Wi-Fi networks discovered by NetworkManager
- filters Wi-Fi networks by `pop-launcher` query (e.g. search for "my" and see "my fancy network" in the results)
- start your search with "wifi" to scan for all available Wi-Fi networks
- selection of a Wi-Fi network causes NetworkManager to attempt to connect to it

### VPN

- returns all VPN connections known to NetworkManager
- filters VPN connections by `pop-launcher` query (e.g. search for "my" and see "my fancy tunnel" in the results)
- search by connection type e.g. "vpn" to see VPN connections
- selection causes NetworkManager to connect/disconnect as appropriate

## getting started

1. install: `cargo install --git https://gitlab.com/jokeyrhyme/pop-launcher-plugin-networkmanager-rs.git`

1. install into ~/.local/share/pop-launcher/plugins: `pop-pop-launcher-plugin-networkmanager install --user`

## contributing

- adhere to https://www.conventionalcommits.org/en/v1.0.0/
- adhere to https://semver.org/

## see also

- https://github.com/oknozor/onagre
- https://github.com/pop-os/launcher
- https://networkmanager.dev/
