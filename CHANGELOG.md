# Changelog

We document all notable changes to this project in this file.

We base the format on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- `install --user` CLI arguments to install plugin files where `pop-launcher` will find them
- include connection type in the entry description
- include connection type in the entry tags
- sort results by relevance, prefering a matching connection type as the first term (e.g. query starts with "wifi"),
  and prefering connections where connection names include search terms (e.g. query "app" matches "apple" network)

### Fixed

- don't block on waiting for Wi-Fi scanning unless query starts with "wifi"
- show Wi-Fi networks once, instead of showing both a "connect" and "disconnect" entries
